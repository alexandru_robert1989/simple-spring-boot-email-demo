
# Simple Spring Boot email demo 

This project allows a user sending emails thanks mainly to the Spring Boot Starter Mail dependency and 
the Swagger UI framework.

The exposed endpoints allows sending emails with or without attachments. 

Please note that this project represents a simple example. 

You can improve the Swagger UI implementation and implement security measures if you intend on using it in one of your projects.


## Testing the app 

First of all, you will need to complete your gmail credentials inside  the _application.properties_ file :
 
spring.mail.username=yourEmail@gmail.com
spring.mail.password=yourPasswordOrAppPassword

You are free to try setting an email connexion using a different email provider.


Launch the project than go to : 

http://localhost:8080/swagger-ui.html

 that will redirect you to 

http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

![Endpoints](doc_images/endpoints.png?raw=true "Endpoints")


You can use the send-simple-email endpoint to send an email without any attachments.
Click on the _Try it out_ button, complete the request body and _Execute_.
Example of valid value for sending a request : 

_{
"subject": "Testing my email app",
"content": "Well, I hope you will receive my message ...",
"recipients": [
"email1@email.com",
"emai21@email.com"]
}_

The subject value is optional.


You can use the send-email endpoint to send an email with or without attachments.

Click on the _Try it out_ button, complete the request body, optionally add _attachments_ and _Execute_.

![Endpoints](doc_images/email_endpoint.png?raw=true "Endpoints")


***Note*** The attachments input falsely indicates _Add string item_. When clicking the message,
you will add a file. 

![Endpoints](doc_images/attachments_input.png?raw=true "Endpoints")
