package com.example.simplespringbootmaildemo.domain;


import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Builder
public class Email {

    @ApiModelProperty(notes = "The email subject",
            required = false,
            allowEmptyValue = true,
            value = "Response to job offer number 123456",
            example = "Response to job offer number 123456")
    private  String subject;



    @NotBlank(message = "The email content cannot be empty")
    @ApiModelProperty(notes = "The email content",
            required = true,
            allowEmptyValue = false,
            value = "Dear head of human resources department ... ",
            example = "Dear head of human resources department ... ")
    private  String content;


    @NotEmpty(message = "The email recipients list cannot be empty")
    @ApiModelProperty(notes = "The email recipients", required = true,
            dataType = "java.util.List<String>",
            value = "[email1@email.com, email2@email.com, emailn@email.com]",
            example = "[email1@email.com, email2@email.com, emailn@email.com]")
    private  List<String> recipients;



}
