package com.example.simplespringbootmaildemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSpringBootMailDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleSpringBootMailDemoApplication.class, args);

        System.out.println("I work ");
    }

}
