package com.example.simplespringbootmaildemo.config;


import com.example.simplespringbootmaildemo.exception.ConstraintViolationExceptionResponse;
import com.example.simplespringbootmaildemo.exception.MethodArgumentNotValidExceptionResponse;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;


@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {SizeLimitExceededException.class})
    protected ResponseEntity<?> handleSizeLimitExceededException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body("Attachment file limit exceeded :\n" + ex.getMessage());
    }


    @ExceptionHandler(value = {MessagingException.class})
    protected ResponseEntity<?> handleMessagingException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body("Sending email failed :\n" + ex.getMessage());
    }


    @ExceptionHandler(value = {javax.mail.SendFailedException.class})
    protected ResponseEntity<?> handleSendFailedException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body("Sending email failed :\n" + ex.getMessage());
    }


    @ExceptionHandler(value = {com.sun.mail.smtp.SMTPAddressFailedException.class})
    protected ResponseEntity<?> handleSMTPAddressFailedException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body("Sending email failed :\n" + ex.getMessage());
    }


    @ExceptionHandler(value = {org.springframework.mail.MailSendException.class})
    protected ResponseEntity<?> handleMailSendException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body("Sending email failed :\n" + ex.getMessage());
    }


    @ExceptionHandler(value = {java.lang.IllegalArgumentException.class})
    protected ResponseEntity<?> handleIllegalArgumentException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.badRequest().body("Sending email failed :\n" + ex.getMessage());
    }


    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers, HttpStatus status, WebRequest request) {
        return ResponseEntity.badRequest().body("Invalid method arguments :\n" + ex.getMessage());
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        return MethodArgumentNotValidExceptionResponse.generate(ex);
    }


    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<?> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        return ConstraintViolationExceptionResponse.generate(ex);
    }

}
