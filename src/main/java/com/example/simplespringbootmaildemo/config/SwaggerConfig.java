package com.example.simplespringbootmaildemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .build()
                .useDefaultResponseMessages(Boolean.FALSE)
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfoBuilder().title("Spring Boot Starter email demo ")
                .description("This API presents the endpoints used to send emails with or without attachments")
                .contact(new Contact("Alexandru DRAGOMIR", "https://www.psypec.fr", "fake@email.com"))
                .version("1.0.0")
                .build();
    }

}
