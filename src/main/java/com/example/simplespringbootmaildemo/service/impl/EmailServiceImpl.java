package com.example.simplespringbootmaildemo.service.impl;

import com.example.simplespringbootmaildemo.domain.Email;
import com.example.simplespringbootmaildemo.exception.AttachmentSizeException;
import com.example.simplespringbootmaildemo.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    /**
     * Object used for sending an email.
     */
    private final JavaMailSender javaMailSender;


    @Value("${max.file.size.mb}")
    private String maxAttachmentsSizeMB;


    /**
     * Send a simple email, without any attachments, to one or more recipients.
     *
     * @param email an {@link Email} object
     * @return a {@link String} value indicating if the operation succeeded or not.
     * @throws MessagingException one of the exceptions thrown if sending email fails. The other potentials exceptions are handled by
     *                            the {@link com.example.simplespringbootmaildemo.config.CustomExceptionHandler} class.
     */
    @Override
    public String sendSimpleEmail(Email email) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // multipart value is false because no attachments will be added
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false);
        helper.setSubject(subject(email));
        helper.setText(email.getContent(), true);
        helper.setTo(recipients(email.getRecipients()));
        javaMailSender.send(mimeMessage);
        return successfullySentEmailResponse(email);
    }


    /**
     * Send an email, with or without attachments, to one or more recipients.
     *
     * @param email an {@link Email} object
     * @param files a {@link List} of {@link MultipartFile}s, representing the email attachments
     * @return a {@link String} value indicating if the operation succeeded or not.
     * @throws MessagingException one of the exceptions thrown if sending email fails. The other potentials exceptions are handled by
     *                            the {@link com.example.simplespringbootmaildemo.config.CustomExceptionHandler} class.
     */
    @Override
    public String sendEmail(Email email, List<MultipartFile> files) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // multipart value is false because  attachments can be added
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setSubject(subject(email));
        helper.setText(email.getContent(), true);
        helper.setTo(recipients(email.getRecipients()));
        addAttachments(helper, files);
        javaMailSender.send(mimeMessage);
        return successfullySentEmailResponse(email);
    }


    /**
     * @param helper a {@link MimeMessageHelper}, use to enrich a {@link MimeMessage}
     * @param files  a {@link List} of {@link MultipartFile}s, representing the email attachments.
     * @throws AttachmentSizeException exception thrown if maximum attachments size was exceeded
     * @throws MessagingException      one of the exceptions thrown if sending email fails. The other potentials exceptions are handled by
     *                                 the {@link com.example.simplespringbootmaildemo.config.CustomExceptionHandler} class.
     */
    @Override
    public void addAttachments(MimeMessageHelper helper, List<MultipartFile> files)
            throws AttachmentSizeException, MessagingException {

        if (files == null || files.isEmpty()) {
            return;
        }

        //retrieve max attachments size in MB, declared inside the properties file
        double attachmentsMaxSizeMb = Double.parseDouble(maxAttachmentsSizeMB);

        //calculate files total size in MB
        double attachmentsSize = files.stream().map(f -> f.getSize() * 0.00000095367432).reduce(0D, Double::sum);

        if (attachmentsSize > attachmentsMaxSizeMb) {
            throw new AttachmentSizeException("You can upload maximum " + maxAttachmentsSizeMB + " MB of files. You are at " + attachmentsSize + " MB");
        }

        //add all files as attachments
        for (MultipartFile file : files) {
            helper.addAttachment(Objects.requireNonNull(file.getOriginalFilename()), file);
        }

    }


    /*
    Keep only unique email addresses
     */
    private String[] recipients(List<String> recipients) {
        //
        List<String> uniqueRecipients = recipients.stream().distinct().collect(Collectors.toList());
        return uniqueRecipients.toArray(new String[0]);
    }


    /*
    Return email subject avoiding NullPointerException
     */
    private String subject(Email email) {
        return StringUtils.isNoneBlank(email.getSubject()) ? email.getSubject() : "";
    }


    /*
    Build the message returned to the user if sending the email succeeds.
     */
    private String successfullySentEmailResponse(Email email) {
        StringBuilder response = new StringBuilder();
        response.append("Email sent to : ");
        email.getRecipients().stream().distinct().forEach(r -> {
            response.append("\n\t").append(r);
        });
        response.append("\n\nEmail subject : ").append(subject(email));
        response.append("\n\nEmail content : " + "\n\n").append(email.getContent());
        return response.toString();
    }


}
