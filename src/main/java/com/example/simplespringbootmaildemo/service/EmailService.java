package com.example.simplespringbootmaildemo.service;


import com.example.simplespringbootmaildemo.domain.Email;
import com.example.simplespringbootmaildemo.exception.AttachmentSizeException;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.util.List;

/**
 * Interface providing the abstract methods that can be implemented
 * to send emails, with or without attachments.
 */
public interface EmailService {


    String sendSimpleEmail(Email email) throws MessagingException;

    String sendEmail(Email email, List<MultipartFile> fileList) throws MessagingException;

    void addAttachments( MimeMessageHelper helper,List<MultipartFile>files) throws AttachmentSizeException, MessagingException;



}
