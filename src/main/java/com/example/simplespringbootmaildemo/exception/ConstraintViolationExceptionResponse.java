package com.example.simplespringbootmaildemo.exception;


import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class providing a method that can be used to return a
 * response when a {@link ConstraintViolationException} occurs.
 */
@Log4j2
public class ConstraintViolationExceptionResponse {

    /**
     * This methos returns a {@link ResponseEntity} containing a
     * {@link Map} within its body. The {@link Map} will contain  constraint
     * violations messages, having as keys the name of the properties
     * having caused the exception.
     *
     * @param e a {@link ConstraintViolationException}
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<?> generate(ConstraintViolationException e) {

        log.info(e.getStackTrace());

        Map<String, String> constraintsViolationsMap = new HashMap<>();

        Set<ConstraintViolation<?>> erreursValidation = e.getConstraintViolations();

        erreursValidation.forEach(constraintViolation -> {
            constraintsViolationsMap.put(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
        });

        return ResponseEntity.badRequest().body(constraintsViolationsMap);
    }
}
