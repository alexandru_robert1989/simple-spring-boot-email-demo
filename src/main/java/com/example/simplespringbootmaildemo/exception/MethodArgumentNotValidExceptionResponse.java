package com.example.simplespringbootmaildemo.exception;


import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class providing a method that can be used to return a
 * response when a {@link ConstraintViolationException} occurs.
 */
@Log4j2
public class MethodArgumentNotValidExceptionResponse {

    /**
     * This methos returns a {@link ResponseEntity} containing a
     * {@link Map} within its body. The {@link Map} will contain  constraint
     * violations messages, having as keys the name of the properties
     * having caused the exception.
     *
     * @param e a {@link ConstraintViolationException}
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<Object> generate(MethodArgumentNotValidException e) {

        log.debug(e.getStackTrace());

        Map<String, String> constraintsViolationsMap = new HashMap<>();

        List<FieldError> fieldErrorList = e.getBindingResult().getFieldErrors();

        fieldErrorList.forEach(err -> {
            constraintsViolationsMap.put(err.getField(), err.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(constraintsViolationsMap);
    }
}
