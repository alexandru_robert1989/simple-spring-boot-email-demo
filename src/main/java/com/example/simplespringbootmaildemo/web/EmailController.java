package com.example.simplespringbootmaildemo.web;

import com.example.simplespringbootmaildemo.domain.Email;
import com.example.simplespringbootmaildemo.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("emails")
@Api("Email endpoints")
@CrossOrigin(value = "*")
@RequiredArgsConstructor
public class EmailController {


    private final EmailService emailService;


    /**
     * Method e used to check if the email controller is up and running
     *
     * @return a {@link String} message
     */
    @GetMapping("/test-page")
    @ApiOperation("Check if the email controller is up and running")
    public ResponseEntity<?> emailsTestPage() {
        return ResponseEntity.ok("Email controller is online");
    }


    /**
     * Method allowing to send a simple email, without attachments, to one or more recipients.
     *
     * @param email an {@link Email} object
     * @return a {@link String} message indicating if the operation succeeded or not.
     * @throws MessagingException one of the exceptions thrown if sending email fails. The other potentials exceptions are handled by
     *                            the {@link com.example.simplespringbootmaildemo.config.CustomExceptionHandler} class.
     */
    @ApiOperation("Send a simple email, without attachments")
    @PostMapping("/send-simple-email")
    public ResponseEntity<?> sendSimpleEmail(@RequestBody @Valid Email email) throws MessagingException {
        return ResponseEntity.ok(emailService.sendSimpleEmail(email));
    }


    /**
     * Method allowingt to send an email, with our without attachments, to one or more recipients.
     *
     * @param files           a {@link List} of {@link MultipartFile}s, representing the email attachments.
     * @param emailObject     a {@link String} value representing the email objet.
     * @param emailContent    a {@link String} value representing the email content.
     * @param emailRecipients an array of {@link String} values representing the email recipients.
     * @return a {@link String} value indicating if the operation succeeded or not.
     * @throws MessagingException one of the exceptions thrown if sending email fails. The other potentials exceptions are handled by
     *                            the {@link com.example.simplespringbootmaildemo.config.CustomExceptionHandler} class.
     */
    @PostMapping(value = "/send-email", consumes = {"multipart/form-data"})
    @ApiOperation("Send an email with or without attachments")
    public ResponseEntity<?> sendEmail(@RequestParam(required = false, value = "add attachments") List<MultipartFile> files,
                                       @RequestParam(required = false, value = "Response to job offer number 123456") String emailObject,
                                       @RequestParam(value = "Dear head of human resources department ... ") String emailContent,
                                       @RequestParam(value = "email1@email.com, email2@email.com, ..., emailn@email.com")
                                               List<String> emailRecipients) throws MessagingException {
        Email email = Email.builder()
                .subject(emailObject)
                .content(emailContent)
                .recipients(emailRecipients)
                .build();

        return ResponseEntity.ok(emailService.sendEmail(email, files));
    }


}
